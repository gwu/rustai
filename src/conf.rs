extern crate toml;

// use std::io;
use std::path::Path;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    pub location: String,
    pub query: Vec<String>,
    pub blacklist: Vec<String>,
    // whitelist: Vec<String>,
    pub maxpages: i64,
}

const CONFIG_DEFAULT_PATH: &str = "~/.config/rustai/conf.toml";

impl Default for Config {
    fn default() -> Config {
        Config{
            location: String::from("~/img/djn"),
            query: vec![String::from("english asanagi")],
            blacklist: vec![String::from("scat")],
            // whitelist: vec![String::from("")],
            maxpages: -1,
        }
    }
}

pub fn ld(p: Option<&str>) -> Config {
    let p = p.unwrap_or(CONFIG_DEFAULT_PATH);
    if Path::new(p).exists() {
        write_default(p);
    };

    Default::default()
}

pub fn write_default(p: &str) {
    // let cfg: Config = Default::default();

    print!("config '{}' not found", p);
}
