#[macro_use]
extern crate serde_derive;

extern crate clap;
extern crate nhentai;

mod conf;

fn main() {
    let m = build_cli().get_matches();
    let cfg = conf::ld(m.value_of("config"));

    for q in m.values_of("search").unwrap_or_default() {
        nhentai::search(&[q, " -", &cfg.blacklist.join(" -")[..]].concat())
    }

    for g in m.values_of("gallery").unwrap_or_default() {
        println!("{:?}", nhentai::gallery(g));
    }
}

/// build command arguments
fn build_cli<'a, 'b>() -> clap::App<'a, 'b> {
    use clap::{crate_description, crate_name, crate_version, App, Arg};

    App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .arg(Arg::with_name("config")
            .long("config")
            .short("c")
            .takes_value(true)
            .help("specify configuration/query file")
        )
        .arg(Arg::with_name("search")
            .long("search")
            .short("s")
            .help("search for queries")
            .takes_value(true)
            .multiple(true)
            .value_name("querie(s)")
        )
        .arg(Arg::with_name("gallery")
            .long("gallery")
            .short("g")
            .help("search for galleries")
            .takes_value(true)
            .multiple(true)
            .value_name("gallery id(s)")
        )
}
